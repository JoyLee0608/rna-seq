version 1.0

task rsem {
    input {
        File transcriptome_bam
        File rsem_reference
        String prefix

        Int memory
        Int disk_space
        Int num_threads
        Boolean num_preempt

        Int? max_frag_len
        String? estimate_rspd
        String? is_stranded
        String? paired_end
        String? calc_ci
    }
    
    command {
        set -e pipefail
        
        # etract inde  
        mkdir -p rsem_reference
        tar -vxf ${rsem_reference} -C rsem_reference --strip-components=1

        /src/run_RSEM.py \
            ${"--max_frag_len " + max_frag_len} \
            ${"--estimate_rspd " + estimate_rspd} \
            ${"--is_stranded " + is_stranded} \
            ${"--paired_end " + paired_end} \
            ${"--calc_ci " + calc_ci} \
            --threads ${num_threads} \
            rsem_reference ${transcriptome_bam} ${prefix}
        gzip *.results
    }

    output {
        File genes="${prefix}.rsem.genes.results.gz"
        File isoforms="${prefix}.rsem.isoforms.results.gz"
    }

    runtime {
        docker: "bioos-cn-guangzhou.cr.volces.com/gcbi/gtex-rnaseq:V10"
        memory: "${memory}GB"
        disk: "${disk_space}GB"
        cpu: "${num_threads}"
        preemptible: "${num_preempt}"
        continueOnReturnCode: 0
    }

    meta {
        author: "David Wu"
    }
}


workflow rsem_workflow {
    call rsem
}
