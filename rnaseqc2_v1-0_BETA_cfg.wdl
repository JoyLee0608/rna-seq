version 1.0

task rnaseqc2 {
    input {
        File bam_file
        File genes_gtf
        String sample_id
        String? strandedness 
        File? intervals_bed

        Int memory
        Int disk_space
        Int num_threads
        Boolean num_preempt
    }

    command {
        set -e pipefail
        echo $(date +"[%b %d %H:%M:%S] Running RNA-SeQC 2")
        touch ${sample_id}.fragmentSizes.txt
        
        /src/run_rnaseqc.py \
            ${genes_gtf} ${bam_file} ${sample_id} \
            -o . \
            ${"--bed " + intervals_bed} \
            ${"--stranded " + strandedness} 

        echo "  * compressing outputs"
        gzip *.gct
        echo $(date +"[%b %d %H:%M:%S] done")
    }

    output {
        File gene_tpm = "${sample_id}.gene_tpm.gct.gz"
        File gene_counts = "${sample_id}.gene_reads.gct.gz"
        File exon_counts = "${sample_id}.exon_reads.gct.gz"
        File metrics = "${sample_id}.metrics.tsv"
        File insertsize_distr = "${sample_id}.fragmentSizes.txt"
    }

    runtime {
        docker: "bioos-cn-guangzhou.cr.volces.com/gcbi/gtex-rnaseq:V10"
        memory: "${memory}GB"
        disk: "${disk_space}GB"
        cpu: "${num_threads}"
        preemptible: "${num_preempt}"
        continueOnReturnCode: 0
    }

    meta {
        author: "Francois Aguet"
    }
}


workflow rnaseqc2_workflow {
    call rnaseqc2
}
